.. scm documentation master file, created by
   sphinx-quickstart on Tue Jul 13 16:14:35 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Stochastic Compartmental Models **scm**
=======================================

**scm** is a python package for generating samples from stochastic compartmental models whose system of differential equations can be described in terms of a stoichiometry matrix.  This package is available in the `scm <https://gitlab.com/achale/scm>`_ repo.

This web page gives two examples of using this package:

#. SIR epidemic model using chain Binomial algorithm
#. Predator–prey (Lotka-Volterra) model using Gillespie algorithm

**scm** can be installed using pip:

.. code-block:: bash

   $ pip install git+https://gitlab.com/achale/scm.git


Note that the algorithms in this package were developed at different times to serve very different purposes hence there are some differences in coding styles.  Although not discussed on this web page one common driving force was to develop unit tests.


.. toctree::
   :maxdepth: 2
   :caption:  scm:



1: SIR model using chain Binomial algorithm
===================================================

The SIR model relates to epidemics in which there are :math:`\mathcal{N}` individuals.  Each individual is assigned to one of three compartments: (S) susceptible but not yet infected, (I) infectious and (R) recovered.  During the course of the epidemic individuals may move forward between compartments, specifically from S to I and from I to R.  With :math:`S(t)`, :math:`I(t)` and :math:`R(t)` representing the number of individuals in each compartment at time :math:`t` then the system of differential equations for this epidemic SIR model is

.. math::
	\frac{d}{dt}S(t) & = \frac{-\beta I(t)}{\mathcal{N}} S(t)
	
	\frac{d}{dt}I(t) & = \frac{ \beta I(t)}{\mathcal{N}} S(t) - \gamma I(t)
	
	\frac{d}{dt}R(t) & = \gamma I(t)

with transition rate parameters :math:`\beta` and :math:`\gamma`.  These parameters control the rate at which individuals move between the compartments: :math:`\beta` relates to S :math:`\rightarrow` I and :math:`\gamma` to I :math:`\rightarrow` R.  The corresponding stoichiometry matrix :math:`S` for this system can be constructed as follows:

.. math::
	S = \begin{bmatrix}
		-1 & 0 & 0 \\
		1 & -1 & 0 \\
		0 & 1 & 0
	\end{bmatrix}

where the rows represent the states (S, I and R) and the columns represent the transitions between states: S :math:`\rightarrow` I, I :math:`\rightarrow` R and R :math:`\rightarrow` nowhere.


In python import the required packages:

.. literalinclude:: ../../examples/sir_binomial.py
   :lines: 15-18, 21-22

Initialise variables:

.. literalinclude:: ../../examples/sir_binomial.py
   :lines: 24-30

Define the transition rates:

.. literalinclude:: ../../examples/sir_binomial.py
   :lines: 33-37

Define stoichiometry matrix:

.. literalinclude:: ../../examples/sir_binomial.py
   :lines: 40-45
   
Define number cases entering and leaving the SIR model. Assuming there is no emigration or immigration then:

.. literalinclude:: ../../examples/sir_binomial.py
   :lines: 48-52

In this example a chain Binomial algorithm will be used and as such the transitions probabilities must be defined:

.. literalinclude:: ../../examples/sir_binomial.py
   :lines: 55, 67-69
   
Having defined all variables the tensors are initialised with:

.. literalinclude:: ../../examples/sir_binomial.py
   :lines: 73-76

Integration using the chain Binomial algorithm is achieved as follows: 

.. literalinclude:: ../../examples/sir_binomial.py
   :lines: 80-83

The resulting simulations may then be plotted using:

.. literalinclude:: ../../examples/sir_binomial.py
   :lines: 86



.. plot::

	>>> from scm import initialize_tensors
	>>> from scm import Pr
	>>> from scm import integrate_bin
	>>> from scm import plot_sir_binomial
	>>> import tensorflow as tf
	>>> dtype = tf.float64
	>>> nsim = 500                  # number of epidemics
	>>> scale_dt = 4                # time scale factor
	>>> dt = 1.0 / scale_dt         # time step size
	>>> nsteps = 100 * scale_dt     # number of time steps
	>>> states = [990, 10, 0]       # [S0, I0, R0]
	>>> N = sum(states)             # population size
	>>> rates = [[0.28],[0.14],[0.0]]
	>>> stoichiometry = [[-1.0,  0.0,  0.0], [ 1.0, -1.0,  0.0], [ 0.0,  1.0,  0.0]]
	>>> external = [ [0.0],[0.0],[0.0]]
	>>> def probs(S, I, R, beta, gamma, zeta, N, dt, nsim):
	>>>	return tf.stack([Pr(dt, I * beta / N),tf.broadcast_to(Pr(dt, gamma), [nsim]), tf.broadcast_to(Pr(dt, zeta), [nsim])])
	>>> (states, condMultinom, idxsMultinom, ratesSum, relativeRatesFull, stoichiometryBinomial, external) = initialize_tensors(dtype, states, rates, stoichiometry, external, nsim)
	>>> (Time, Counts, EventsB, EventsM, EventsP) = integrate_bin(dtype, states, condMultinom, idxsMultinom, ratesSum, relativeRatesFull, stoichiometryBinomial, external, probs, nsteps, N, dt, nsim)
	>>> plot_sir_binomial(Time, Counts, nsteps, nsim)

A python script which includes this example is available in the `scm <https://gitlab.com/achale/scm>`_ repo, see *examples/sir_binomial.py*.  Furthermore a python script for the SIR model using the Gillespie algorithm is also available, see *examples/sir_gillespie.py*. In the following usage of the Gillespie algorithm is described in relation to the predator–prey model.



2: Predator–prey (Lotka-Volterra) model using Gillespie algorithm
=========================================================================

Here the Gillespie algorithm is used to generate samples from the predator–prey, or so called Lotka-Volterra, model. This model has 4 parameters :math:`\alpha`, :math:`\beta`, :math:`\delta` and :math:`\gamma` and is described by the following system of differential equations:

.. math::
	\frac{d}{dt}X(t) & = \alpha X(t) - \beta X(t) Y(t)
	
	\frac{d}{dt}Y(t) & = \delta X(t) Y(t) - \gamma Y(t)

with :math:`t` representing time then :math:`X(t)` represents the number of prey (e.g. rabbits) and :math:`Y(t)` the number of corresponding predators (e.g. foxes).  The corresponding stoichiometry matrix :math:`S` for this system is:

.. math::
	S^{T} = \begin{bmatrix}
		1 & 0 \\
		-1 & 0 \\
		0 & 1 \\
		0 & -1
	\end{bmatrix}

where the columns represent the states (X and Y) and the rows represent the transitions/interactions between the states: :math:`\rightarrow` X, X :math:`\rightarrow`,  :math:`\rightarrow` Y and Y :math:`\rightarrow`.  Note that the second and third row of :math:`S^{T}` relate to the interaction between X and Y therefore when :math:`\beta` = :math:`\delta` these two rows can be combined into a single row [-1 1].


In python import the required packages:

.. literalinclude:: ../../examples/lotka_volterra_gillespie.py
   :lines: 13-14, 16,17
 
Initialise variables:

.. literalinclude:: ../../examples/lotka_volterra_gillespie.py
   :lines: 19-22

Define the transition rates:

.. literalinclude:: ../../examples/lotka_volterra_gillespie.py
   :lines: 32-37

Define stoichiometry matrix:

.. literalinclude:: ../../examples/lotka_volterra_gillespie.py
   :lines: 23-30
   

In constrast to the SIR example above which used the chain Binomial algorithm this Lotka-Volterra example uses the Gillespie algorithm.  Note that in principle the either algorithm could be applied to each of these models.  Given this example is using the Gillespie algorithm then the propensity must be defined:

.. literalinclude:: ../../examples/lotka_volterra_gillespie.py
   :lines: 39, 49-53
   
Integration using the Gillespie algorithm is undertaken as follows:

.. literalinclude:: ../../examples/lotka_volterra_gillespie.py
   :lines: 57-67

The resulting simulations may then be plotted:

.. literalinclude:: ../../examples/lotka_volterra_gillespie.py
   :lines: 71

.. plot::

	>>> from scm import integrate_gil
	>>> from scm import plot_lotka_volterra_gillespie
	>>> import tensorflow as tf
	>>> dtype = tf.float64
	>>> nsteps = 30000  # maximum number of time steps
	>>> nsim = 200      # number of realisations of Lotka-Volterra process
	>>> initial_state = tf.constant([500, 500], dtype=dtype) # initial count for (X,Y)
	>>> stoichiometry = tf.constant([[ 1,  0],[-1,  0],[ 0,  1],[ 0, -1]],dtype=dtype)
	>>> rates = tf.constant([[0.2],[0.0005],[0.0005],[0.1]],dtype=dtype)
	>>> def propensity(X, Y, alpha, beta, delta, gamma, N):
	>>>    return tf.stack([alpha * X, beta * X * Y,delta * X * Y,gamma * Y])
	>>> @tf.function(autograph=True, experimental_compile=True)
	>>> def simulate_one(elems):
	>>>   (imax, times, states) = integrate_gil(initial_state, rates, stoichiometry, nsteps, propensity)
	>>>   return tf.concat([times, states], axis=1)
	>>> timeseries = tf.map_fn( simulate_one, tf.ones([nsim, 1+stoichiometry.shape[1]]), fn_output_signature=dtype )
	>>> plot_lotka_volterra_gillespie(timeseries, nsim)



A python script that includes this example is available in the `scm <https://gitlab.com/achale/scm>`_ repo, see *examples/lotka_volterra_gillespie.py* .



Acknowledgements
================

Thanks should go to both the Wellcome Trust *GEM: translational software for outbreak analysis* and also UKRI *JUNIPER (Joint UNIversities Pandemic and Epidemiological Research) Consortium* for funding this research.




