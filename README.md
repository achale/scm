# Stochastic Compartmental Models - `scm`

Python package for general stochastic compartmental models.

This package's chain Binomial and Gillespie algorithms are general and will solve systems of ODEs which can be described by a stoichiometry matrix: note that branching is not supported. A basic description, with examples, of `scm` is given at [https://achale.gitlab.io/scm/](https://achale.gitlab.io/scm/)

## Install `scm`

The `scm` package can be installed using pip:

```
pip install git+https://gitlab.com/achale/scm.git
```


## Examples

The `examples` directory contains 3 applications of this package's functions:
- `sir_binomial.py` SIR epidemic model using chain Binomial algorithm
- `sir_gillespie.py` SIR epidemic model using Gillespie algorithm
- `lotka_volterra_gillespie.py` preditor-prey model using Gillespie algorithm

The "SIR using chain Binomial" and "preditor-prey using Gillespie" models are also described in relation to `scm` at [https://achale.gitlab.io/scm/](https://achale.gitlab.io/scm/)

The examples include bespoke unit tests (`scm/unittests.py`) based on analytical solutions from the relevant system of ODEs.

In a future version of `scm` the bespkoke plotting functions could be upgraded into a single general function :)
