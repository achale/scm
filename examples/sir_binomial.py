""" SIR model using chian Binomial algorithm

    Model with parameters beta and gamma:

       dS/dt = -beta/N I(t) S(t)
       dI/dt =  beta/N I(t) S(t) - gamma I(t)
       dR/dt = gamma I(t)
       
       S(t) number of susceptible individuals at time t
       I(t) number of infected individuals at time t
       R(t) number of recovered individuals at time t 
       N = S(t) + I(t) + R(t)
       
"""
from scm import initialize_tensors
from scm import Pr
from scm import integrate_bin
from scm import plot_sir_binomial
from scm import r0_distribution_sir
from scm import r0_report

import tensorflow as tf

dtype = tf.float64
nsim = 500                  # number of epidemics
scale_dt = 4                # time scale factor
dt = 1.0 / scale_dt         # time step size
nsteps = 100 * scale_dt     # number of time steps
states = [990, 10, 0]       # [S0, I0, R0]
N = sum(states)             # population size

# transition rates
rates = [
    [0.28],                 # beta S->I
    [0.14],                 # gamma I->R
    [0.0]                   # R->nowhere (i.e. no individuals leave R)
]

# connectivity between compartments for an SIR model
stoichiometry = [
    # S->I; I->R; R->nowhere
    [-1.0,  0.0,  0.0],  # S
    [ 1.0, -1.0,  0.0],  # I
    [ 0.0,  1.0,  0.0],  # R
]

# rates in/out of each state from external source for 3 state parts (i.e. S, I, R)
external = [
    [0.0],   # S
    [0.0],   # I
    [0.0]    # R
]


def probs(S, I, R, beta, gamma, zeta, N, dt, nsim):
    """ Transition probability functions where Pr(dt,rate)=1-exp(-rate x dt)
    :param S: (tensor) number of susceptible individuals
    :param I: (tensor) number of infected individuals
    :param R: (tensor) number of recovered individuals
    :param beta: (float) model parameter (transition S->I)
    :param gamma: (float) model parameter (transition I->R)
    :param zeta: (float) redundent model parameter (transition R to nowhere)
    :param N: (integer) total number of individuals
    :param dt (float) size of time step
    :param nsim (integer): number of simulations to run simultaneously
    """
    return tf.stack([Pr(dt, I * beta / N),                     # S->I
                     tf.broadcast_to(Pr(dt, gamma), [nsim]),   # I->R
                     tf.broadcast_to(Pr(dt, zeta), [nsim])])   # R->nowhere


# initialise tensors
(states, condMultinom, idxsMultinom, 
 ratesSum, relativeRatesFull,
 stoichiometryBinomial, external) = initialize_tensors(dtype, states, rates, 
                                                       stoichiometry, external, nsim)


# integrate using chain binomial algorithm
(Time, Counts, EventsB, EventsM, 
 EventsP) = integrate_bin(dtype, states, condMultinom, idxsMultinom, 
                          ratesSum, relativeRatesFull, stoichiometryBinomial, 
                          external, probs, nsteps, N, dt, nsim)

# plot function for spcifically for this SIR model
plot_sir_binomial(Time, Counts, nsteps, nsim)


# unit test based on the distribution of the reproduction number r0
S0 = states[0, -3]
R0 = states[0, -1]
St = tf.transpose(Counts[:, -3, :])
It = tf.transpose(Counts[:, -2, :])
Rt = tf.transpose(Counts[:, -1, :])

(r0_sim, r0_mean_per_sim, r0_sim_all) = r0_distribution_sir(S0, R0, St, It, Rt, N)

# when the chain binomial algorithm is working correctly the actual value of
# the reproduction number r0 will be very close to the mode of the distribution 
# of the estimated distribution of r0
r0_report(r0_sim, r0_mean_per_sim, rates)
