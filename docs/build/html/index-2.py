from scm import integrate_gil
from scm import plot_lotka_volterra_gillespie
import tensorflow as tf
dtype = tf.float64
nsteps = 30000  # maximum number of time steps
nsim = 200      # number of realisations of Lotka-Volterra process
initial_state = tf.constant([500, 500], dtype=dtype) # initial count for (X,Y)
stoichiometry = tf.constant([[ 1,  0],[-1,  0],[ 0,  1],[ 0, -1]],dtype=dtype)
rates = tf.constant([[0.2],[0.0005],[0.0005],[0.1]],dtype=dtype)
def propensity(X, Y, alpha, beta, delta, gamma, N):
   return tf.stack([alpha * X, beta * X * Y,delta * X * Y,gamma * Y])
@tf.function(autograph=True, experimental_compile=True)
def simulate_one(elems):
  (imax, times, states) = integrate_gil(initial_state, rates, stoichiometry, nsteps, propensity)
  return tf.concat([times, states], axis=1)
timeseries = tf.map_fn( simulate_one, tf.ones([nsim, 1+stoichiometry.shape[1]]), fn_output_signature=dtype )
plot_lotka_volterra_gillespie(timeseries, nsim)
