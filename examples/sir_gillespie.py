""" SIR model using Gillespie algorithm

    Model with parameters beta and gamma:

       dS/dt = -beta/N I(t) S(t)
       dI/dt =  beta/N I(t) S(t) - gamma I(t)
       dR/dt = gamma I(t)
       
       S(t) number of susceptible individuals at time t
       I(t) number of infected individuals at time t
       R(t) number of recovered individuals at time t 
       N = S(t) + I(t) + R(t)
       
"""

from scm import integrate_gil
from scm import plot_sir_gillespie
from scm import r0_distribution_sir
from scm import r0_report

import tensorflow as tf

dtype = tf.float64
nsteps = 1700   # maximum number of time steps
nsim = 500      # number of realisations of the epidemic process
initial_state = tf.constant([990, 10, 0], dtype=dtype) # initial values for (S, I, R)
stoichiometry =  tf.constant(
    #  S   I   R
    [[-1,  1,  0],   # S->I
     [ 0, -1,  1]],  # I->R
     dtype=dtype
)
rates = tf.constant(
    [[0.28],   # beta:  S->I
     [0.14]],  # gamma: I->R
    dtype=dtype)


def propensity(S, I, R, beta, gamma, N):
    """ Propensity for SIR model
    :param S: (tensor) number of susceptible individuals
    :param I: (tensor) number of infected individuals
    :param R: (tensor) number of recovered individuals
    :param beta: (float) model parameter (transition S->I)
    :param gamma: (float) model parameter (transition I->R)
    :param N: (integer) total number of individuals
    """
    return tf.stack(
        [(beta * S * I) / N,  # S->I
          gamma * I ])        # I->R


# integrate using Gillespie algorithm
@tf.function(autograph=True, experimental_compile=True)
def simulate_one(elems):
    """ One realisation of epidemic process """
    (imax, times, states) = integrate_gil(initial_state, rates, stoichiometry, nsteps, propensity)
    return tf.concat([times, states], axis=1)

timeseries = tf.map_fn(
    simulate_one,
    tf.ones([nsim, 1+stoichiometry.shape[1]]),
    fn_output_signature=dtype
)


# plot function for spcifically for this SIR model
plot_sir_gillespie(timeseries, nsim)


# unit test based on the distribution of the reproduction number r0
S0 = initial_state[-3]
R0 = initial_state[-1]
St = timeseries[:, :, -3]
It = timeseries[:, :, -2]
Rt = timeseries[:, :, -1]
N = tf.reduce_sum(initial_state)

(r0_sim, r0_mean_per_sim, r0_sim_all) = r0_distribution_sir(S0, R0, St, It, Rt, N)

# when the Gillespie algorithm is working correctly the actual value of
# the reproduction number r0 will be very close to the mode of the distribution 
# of the estimated distribution of r0
r0_report(r0_sim, r0_mean_per_sim, rates.numpy())
