
<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" /><meta name="generator" content="Docutils 0.17.1: http://docutils.sourceforge.net/" />

    <title>Stochastic Compartmental Models scm &#8212; scm 0.0.2 documentation</title>
    <link rel="stylesheet" type="text/css" href="_static/pygments.css" />
    <link rel="stylesheet" type="text/css" href="_static/haiku.css" />
    <link rel="stylesheet" type="text/css" href="_static/plot_directive.css" />
    <script data-url_root="./" id="documentation_options" src="_static/documentation_options.js"></script>
    <script src="_static/jquery.js"></script>
    <script src="_static/underscore.js"></script>
    <script src="_static/doctools.js"></script>
    <script async="async" src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
    <link rel="index" title="Index" href="genindex.html" />
    <link rel="search" title="Search" href="search.html" /> 
  </head><body>
      <div class="header" role="banner"><h1 class="heading"><a href="#">
          <span>scm 0.0.2 documentation</span></a></h1>
        <h2 class="heading"><span>Stochastic Compartmental Models scm</span></h2>
      </div>
      <div class="topnav" role="navigation" aria-label="top navigation">
      
        <p>
        <a class="uplink" href="#">Contents</a>
        </p>

      </div>
      <div class="content" role="main">
        
        
  <section id="stochastic-compartmental-models-scm">
<h1>Stochastic Compartmental Models <strong>scm</strong><a class="headerlink" href="#stochastic-compartmental-models-scm" title="Permalink to this headline">¶</a></h1>
<p><strong>scm</strong> is a python package for generating samples from stochastic compartmental models whose system of differential equations can be described in terms of a stoichiometry matrix.  This package is available in the <a class="reference external" href="https://gitlab.com/achale/scm">scm</a> repo.</p>
<p>This web page gives two examples of using this package:</p>
<ol class="arabic simple">
<li><p>SIR epidemic model using chain Binomial algorithm</p></li>
<li><p>Predator–prey (Lotka-Volterra) model using Gillespie algorithm</p></li>
</ol>
<p><strong>scm</strong> can be installed using pip:</p>
<div class="highlight-bash notranslate"><div class="highlight"><pre><span></span>$ pip install git+https://gitlab.com/achale/scm.git
</pre></div>
</div>
<p>Note that the algorithms in this package were developed at different times to serve very different purposes hence there are some differences in coding styles.  Although not discussed on this web page one common driving force was to develop unit tests.</p>
<div class="toctree-wrapper compound">
</div>
</section>
<section id="sir-model-using-chain-binomial-algorithm">
<h1>1: SIR model using chain Binomial algorithm<a class="headerlink" href="#sir-model-using-chain-binomial-algorithm" title="Permalink to this headline">¶</a></h1>
<p>The SIR model relates to epidemics in which there are <span class="math notranslate nohighlight">\(\mathcal{N}\)</span> individuals.  Each individual is assigned to one of three compartments: (S) susceptible but not yet infected, (I) infectious and (R) recovered.  During the course of the epidemic individuals may move forward between compartments, specifically from S to I and from I to R.  With <span class="math notranslate nohighlight">\(S(t)\)</span>, <span class="math notranslate nohighlight">\(I(t)\)</span> and <span class="math notranslate nohighlight">\(R(t)\)</span> representing the number of individuals in each compartment at time <span class="math notranslate nohighlight">\(t\)</span> then the system of differential equations for this epidemic SIR model is</p>
<div class="math notranslate nohighlight">
\[ \begin{align}\begin{aligned}\frac{d}{dt}S(t) &amp; = \frac{-\beta I(t)}{\mathcal{N}} S(t)\\\frac{d}{dt}I(t) &amp; = \frac{ \beta I(t)}{\mathcal{N}} S(t) - \gamma I(t)\\\frac{d}{dt}R(t) &amp; = \gamma I(t)\end{aligned}\end{align} \]</div>
<p>with transition rate parameters <span class="math notranslate nohighlight">\(\beta\)</span> and <span class="math notranslate nohighlight">\(\gamma\)</span>.  These parameters control the rate at which individuals move between the compartments: <span class="math notranslate nohighlight">\(\beta\)</span> relates to S <span class="math notranslate nohighlight">\(\rightarrow\)</span> I and <span class="math notranslate nohighlight">\(\gamma\)</span> to I <span class="math notranslate nohighlight">\(\rightarrow\)</span> R.  The corresponding stoichiometry matrix <span class="math notranslate nohighlight">\(S\)</span> for this system can be constructed as follows:</p>
<div class="math notranslate nohighlight">
\[\begin{split}S = \begin{bmatrix}
        -1 &amp; 0 &amp; 0 \\
        1 &amp; -1 &amp; 0 \\
        0 &amp; 1 &amp; 0
\end{bmatrix}\end{split}\]</div>
<p>where the rows represent the states (S, I and R) and the columns represent the transitions between states: S <span class="math notranslate nohighlight">\(\rightarrow\)</span> I, I <span class="math notranslate nohighlight">\(\rightarrow\)</span> R and R <span class="math notranslate nohighlight">\(\rightarrow\)</span> nowhere.</p>
<p>In python import the required packages:</p>
<div class="highlight-default notranslate"><div class="highlight"><pre><span></span><span class="kn">from</span> <span class="nn">scm</span> <span class="kn">import</span> <span class="n">initialize_tensors</span>
<span class="kn">from</span> <span class="nn">scm</span> <span class="kn">import</span> <span class="n">Pr</span>
<span class="kn">from</span> <span class="nn">scm</span> <span class="kn">import</span> <span class="n">integrate_bin</span>
<span class="kn">from</span> <span class="nn">scm</span> <span class="kn">import</span> <span class="n">plot_sir_binomial</span>

<span class="kn">import</span> <span class="nn">tensorflow</span> <span class="k">as</span> <span class="nn">tf</span>
</pre></div>
</div>
<p>Initialise variables:</p>
<div class="highlight-default notranslate"><div class="highlight"><pre><span></span><span class="n">dtype</span> <span class="o">=</span> <span class="n">tf</span><span class="o">.</span><span class="n">float64</span>
<span class="n">nsim</span> <span class="o">=</span> <span class="mi">500</span>                  <span class="c1"># number of epidemics</span>
<span class="n">scale_dt</span> <span class="o">=</span> <span class="mi">4</span>                <span class="c1"># time scale factor</span>
<span class="n">dt</span> <span class="o">=</span> <span class="mf">1.0</span> <span class="o">/</span> <span class="n">scale_dt</span>         <span class="c1"># time step size</span>
<span class="n">nsteps</span> <span class="o">=</span> <span class="mi">100</span> <span class="o">*</span> <span class="n">scale_dt</span>     <span class="c1"># number of time steps</span>
<span class="n">states</span> <span class="o">=</span> <span class="p">[</span><span class="mi">990</span><span class="p">,</span> <span class="mi">10</span><span class="p">,</span> <span class="mi">0</span><span class="p">]</span>       <span class="c1"># [S0, I0, R0]</span>
<span class="n">N</span> <span class="o">=</span> <span class="nb">sum</span><span class="p">(</span><span class="n">states</span><span class="p">)</span>             <span class="c1"># population size</span>
</pre></div>
</div>
<p>Define the transition rates:</p>
<div class="highlight-default notranslate"><div class="highlight"><pre><span></span><span class="n">rates</span> <span class="o">=</span> <span class="p">[</span>
    <span class="p">[</span><span class="mf">0.28</span><span class="p">],</span>                 <span class="c1"># beta S-&gt;I</span>
    <span class="p">[</span><span class="mf">0.14</span><span class="p">],</span>                 <span class="c1"># gamma I-&gt;R</span>
    <span class="p">[</span><span class="mf">0.0</span><span class="p">]</span>                   <span class="c1"># R-&gt;nowhere (i.e. no individuals leave R)</span>
<span class="p">]</span>
</pre></div>
</div>
<p>Define stoichiometry matrix:</p>
<div class="highlight-default notranslate"><div class="highlight"><pre><span></span><span class="n">stoichiometry</span> <span class="o">=</span> <span class="p">[</span>
    <span class="c1"># S-&gt;I; I-&gt;R; R-&gt;nowhere</span>
    <span class="p">[</span><span class="o">-</span><span class="mf">1.0</span><span class="p">,</span>  <span class="mf">0.0</span><span class="p">,</span>  <span class="mf">0.0</span><span class="p">],</span>  <span class="c1"># S</span>
    <span class="p">[</span> <span class="mf">1.0</span><span class="p">,</span> <span class="o">-</span><span class="mf">1.0</span><span class="p">,</span>  <span class="mf">0.0</span><span class="p">],</span>  <span class="c1"># I</span>
    <span class="p">[</span> <span class="mf">0.0</span><span class="p">,</span>  <span class="mf">1.0</span><span class="p">,</span>  <span class="mf">0.0</span><span class="p">],</span>  <span class="c1"># R</span>
<span class="p">]</span>
</pre></div>
</div>
<p>Define number cases entering and leaving the SIR model. Assuming there is no emigration or immigration then:</p>
<div class="highlight-default notranslate"><div class="highlight"><pre><span></span><span class="n">external</span> <span class="o">=</span> <span class="p">[</span>
    <span class="p">[</span><span class="mf">0.0</span><span class="p">],</span>   <span class="c1"># S</span>
    <span class="p">[</span><span class="mf">0.0</span><span class="p">],</span>   <span class="c1"># I</span>
    <span class="p">[</span><span class="mf">0.0</span><span class="p">]</span>    <span class="c1"># R</span>
<span class="p">]</span>
</pre></div>
</div>
<p>In this example a chain Binomial algorithm will be used and as such the transitions probabilities must be defined:</p>
<div class="highlight-default notranslate"><div class="highlight"><pre><span></span><span class="k">def</span> <span class="nf">probs</span><span class="p">(</span><span class="n">S</span><span class="p">,</span> <span class="n">I</span><span class="p">,</span> <span class="n">R</span><span class="p">,</span> <span class="n">beta</span><span class="p">,</span> <span class="n">gamma</span><span class="p">,</span> <span class="n">zeta</span><span class="p">,</span> <span class="n">N</span><span class="p">,</span> <span class="n">dt</span><span class="p">,</span> <span class="n">nsim</span><span class="p">):</span>
    <span class="k">return</span> <span class="n">tf</span><span class="o">.</span><span class="n">stack</span><span class="p">([</span><span class="n">Pr</span><span class="p">(</span><span class="n">dt</span><span class="p">,</span> <span class="n">I</span> <span class="o">*</span> <span class="n">beta</span> <span class="o">/</span> <span class="n">N</span><span class="p">),</span>                     <span class="c1"># S-&gt;I</span>
                     <span class="n">tf</span><span class="o">.</span><span class="n">broadcast_to</span><span class="p">(</span><span class="n">Pr</span><span class="p">(</span><span class="n">dt</span><span class="p">,</span> <span class="n">gamma</span><span class="p">),</span> <span class="p">[</span><span class="n">nsim</span><span class="p">]),</span>   <span class="c1"># I-&gt;R</span>
                     <span class="n">tf</span><span class="o">.</span><span class="n">broadcast_to</span><span class="p">(</span><span class="n">Pr</span><span class="p">(</span><span class="n">dt</span><span class="p">,</span> <span class="n">zeta</span><span class="p">),</span> <span class="p">[</span><span class="n">nsim</span><span class="p">])])</span>   <span class="c1"># R-&gt;nowhere</span>
</pre></div>
</div>
<p>Having defined all variables the tensors are initialised with:</p>
<div class="highlight-default notranslate"><div class="highlight"><pre><span></span><span class="p">(</span><span class="n">states</span><span class="p">,</span> <span class="n">condMultinom</span><span class="p">,</span> <span class="n">idxsMultinom</span><span class="p">,</span> 
 <span class="n">ratesSum</span><span class="p">,</span> <span class="n">relativeRatesFull</span><span class="p">,</span>
 <span class="n">stoichiometryBinomial</span><span class="p">,</span> <span class="n">external</span><span class="p">)</span> <span class="o">=</span> <span class="n">initialize_tensors</span><span class="p">(</span><span class="n">dtype</span><span class="p">,</span> <span class="n">states</span><span class="p">,</span> <span class="n">rates</span><span class="p">,</span> 
                                                       <span class="n">stoichiometry</span><span class="p">,</span> <span class="n">external</span><span class="p">,</span> <span class="n">nsim</span><span class="p">)</span>
</pre></div>
</div>
<p>Integration using the chain Binomial algorithm is achieved as follows:</p>
<div class="highlight-default notranslate"><div class="highlight"><pre><span></span><span class="p">(</span><span class="n">Time</span><span class="p">,</span> <span class="n">Counts</span><span class="p">,</span> <span class="n">EventsB</span><span class="p">,</span> <span class="n">EventsM</span><span class="p">,</span> 
 <span class="n">EventsP</span><span class="p">)</span> <span class="o">=</span> <span class="n">integrate_bin</span><span class="p">(</span><span class="n">dtype</span><span class="p">,</span> <span class="n">states</span><span class="p">,</span> <span class="n">condMultinom</span><span class="p">,</span> <span class="n">idxsMultinom</span><span class="p">,</span> 
                          <span class="n">ratesSum</span><span class="p">,</span> <span class="n">relativeRatesFull</span><span class="p">,</span> <span class="n">stoichiometryBinomial</span><span class="p">,</span> 
                          <span class="n">external</span><span class="p">,</span> <span class="n">probs</span><span class="p">,</span> <span class="n">nsteps</span><span class="p">,</span> <span class="n">N</span><span class="p">,</span> <span class="n">dt</span><span class="p">,</span> <span class="n">nsim</span><span class="p">)</span>
</pre></div>
</div>
<p>The resulting simulations may then be plotted using:</p>
<div class="highlight-default notranslate"><div class="highlight"><pre><span></span><span class="n">plot_sir_binomial</span><span class="p">(</span><span class="n">Time</span><span class="p">,</span> <span class="n">Counts</span><span class="p">,</span> <span class="n">nsteps</span><span class="p">,</span> <span class="n">nsim</span><span class="p">)</span>
</pre></div>
</div>
<p>(<a class="reference external" href=".//index-1.py">Source code</a>, <a class="reference external" href=".//index-1.png">png</a>, <a class="reference external" href=".//index-1.hires.png">hires.png</a>, <a class="reference external" href=".//index-1.pdf">pdf</a>)</p>
<figure class="align-default">
<img alt="_images/index-1.png" class="plot-directive" src="_images/index-1.png" />
</figure>
<p>A python script which includes this example is available in the <a class="reference external" href="https://gitlab.com/achale/scm">scm</a> repo, see <em>examples/sir_binomial.py</em>.  Furthermore a python script for the SIR model using the Gillespie algorithm is also available, see <em>examples/sir_gillespie.py</em>. In the following usage of the Gillespie algorithm is described in relation to the predator–prey model.</p>
</section>
<section id="predatorprey-lotka-volterra-model-using-gillespie-algorithm">
<h1>2: Predator–prey (Lotka-Volterra) model using Gillespie algorithm<a class="headerlink" href="#predatorprey-lotka-volterra-model-using-gillespie-algorithm" title="Permalink to this headline">¶</a></h1>
<p>Here the Gillespie algorithm is used to generate samples from the predator–prey, or so called Lotka-Volterra, model. This model has 4 parameters <span class="math notranslate nohighlight">\(\alpha\)</span>, <span class="math notranslate nohighlight">\(\beta\)</span>, <span class="math notranslate nohighlight">\(\delta\)</span> and <span class="math notranslate nohighlight">\(\gamma\)</span> and is described by the following system of differential equations:</p>
<div class="math notranslate nohighlight">
\[ \begin{align}\begin{aligned}\frac{d}{dt}X(t) &amp; = \alpha X(t) - \beta X(t) Y(t)\\\frac{d}{dt}Y(t) &amp; = \delta X(t) Y(t) - \gamma Y(t)\end{aligned}\end{align} \]</div>
<p>with <span class="math notranslate nohighlight">\(t\)</span> representing time then <span class="math notranslate nohighlight">\(X(t)\)</span> represents the number of prey (e.g. rabbits) and <span class="math notranslate nohighlight">\(Y(t)\)</span> the number of corresponding predators (e.g. foxes).  The corresponding stoichiometry matrix <span class="math notranslate nohighlight">\(S\)</span> for this system is:</p>
<div class="math notranslate nohighlight">
\[\begin{split}S^{T} = \begin{bmatrix}
        1 &amp; 0 \\
        -1 &amp; 0 \\
        0 &amp; 1 \\
        0 &amp; -1
\end{bmatrix}\end{split}\]</div>
<p>where the columns represent the states (X and Y) and the rows represent the transitions/interactions between the states: <span class="math notranslate nohighlight">\(\rightarrow\)</span> X, X <span class="math notranslate nohighlight">\(\rightarrow\)</span>,  <span class="math notranslate nohighlight">\(\rightarrow\)</span> Y and Y <span class="math notranslate nohighlight">\(\rightarrow\)</span>.  Note that the second and third row of <span class="math notranslate nohighlight">\(S^{T}\)</span> relate to the interaction between X and Y therefore when <span class="math notranslate nohighlight">\(\beta\)</span> = <span class="math notranslate nohighlight">\(\delta\)</span> these two rows can be combined into a single row [-1 1].</p>
<p>In python import the required packages:</p>
<div class="highlight-default notranslate"><div class="highlight"><pre><span></span><span class="kn">from</span> <span class="nn">scm</span> <span class="kn">import</span> <span class="n">integrate_gil</span>
<span class="kn">from</span> <span class="nn">scm</span> <span class="kn">import</span> <span class="n">plot_lotka_volterra_gillespie</span>

<span class="kn">import</span> <span class="nn">tensorflow</span> <span class="k">as</span> <span class="nn">tf</span>
</pre></div>
</div>
<p>Initialise variables:</p>
<div class="highlight-default notranslate"><div class="highlight"><pre><span></span><span class="n">dtype</span> <span class="o">=</span> <span class="n">tf</span><span class="o">.</span><span class="n">float64</span>
<span class="n">nsteps</span> <span class="o">=</span> <span class="mi">30000</span>  <span class="c1"># maximum number of time steps</span>
<span class="n">nsim</span> <span class="o">=</span> <span class="mi">200</span>      <span class="c1"># number of realisations of Lotka-Volterra process</span>
<span class="n">initial_state</span> <span class="o">=</span> <span class="n">tf</span><span class="o">.</span><span class="n">constant</span><span class="p">([</span><span class="mi">500</span><span class="p">,</span> <span class="mi">500</span><span class="p">],</span> <span class="n">dtype</span><span class="o">=</span><span class="n">dtype</span><span class="p">)</span> <span class="c1"># initial count for (X,Y)</span>
</pre></div>
</div>
<p>Define the transition rates:</p>
<div class="highlight-default notranslate"><div class="highlight"><pre><span></span><span class="n">rates</span> <span class="o">=</span> <span class="n">tf</span><span class="o">.</span><span class="n">constant</span><span class="p">(</span>
    <span class="p">[[</span><span class="mf">0.2</span><span class="p">],</span>    <span class="c1"># -&gt;X</span>
     <span class="p">[</span><span class="mf">0.0005</span><span class="p">],</span> <span class="c1"># X-&gt;</span>
     <span class="p">[</span><span class="mf">0.0005</span><span class="p">],</span> <span class="c1"># -&gt;Y</span>
     <span class="p">[</span><span class="mf">0.1</span><span class="p">]],</span>   <span class="c1"># Y-&gt;</span>
    <span class="n">dtype</span><span class="o">=</span><span class="n">dtype</span><span class="p">)</span>
</pre></div>
</div>
<p>Define stoichiometry matrix:</p>
<div class="highlight-default notranslate"><div class="highlight"><pre><span></span><span class="n">stoichiometry</span> <span class="o">=</span> <span class="n">tf</span><span class="o">.</span><span class="n">constant</span><span class="p">(</span>
    <span class="c1">#  X   Y</span>
    <span class="p">[[</span> <span class="mi">1</span><span class="p">,</span>  <span class="mi">0</span><span class="p">],</span>  <span class="c1"># -&gt;X</span>
     <span class="p">[</span><span class="o">-</span><span class="mi">1</span><span class="p">,</span>  <span class="mi">0</span><span class="p">],</span>  <span class="c1"># X-&gt;</span>
     <span class="p">[</span> <span class="mi">0</span><span class="p">,</span>  <span class="mi">1</span><span class="p">],</span>  <span class="c1"># -&gt;Y</span>
     <span class="p">[</span> <span class="mi">0</span><span class="p">,</span> <span class="o">-</span><span class="mi">1</span><span class="p">]],</span> <span class="c1"># Y-&gt;</span>
     <span class="n">dtype</span><span class="o">=</span><span class="n">dtype</span>
<span class="p">)</span>
</pre></div>
</div>
<p>In constrast to the SIR example above which used the chain Binomial algorithm this Lotka-Volterra example uses the Gillespie algorithm.  Note that in principle the either algorithm could be applied to each of these models.  Given this example is using the Gillespie algorithm then the propensity must be defined:</p>
<div class="highlight-default notranslate"><div class="highlight"><pre><span></span><span class="k">def</span> <span class="nf">propensity</span><span class="p">(</span><span class="n">X</span><span class="p">,</span> <span class="n">Y</span><span class="p">,</span> <span class="n">alpha</span><span class="p">,</span> <span class="n">beta</span><span class="p">,</span> <span class="n">delta</span><span class="p">,</span> <span class="n">gamma</span><span class="p">,</span> <span class="n">N</span><span class="p">):</span>
    <span class="k">return</span> <span class="n">tf</span><span class="o">.</span><span class="n">stack</span><span class="p">(</span>
        <span class="p">[</span><span class="n">alpha</span> <span class="o">*</span> <span class="n">X</span><span class="p">,</span>     <span class="c1"># -&gt;X</span>
         <span class="n">beta</span> <span class="o">*</span> <span class="n">X</span> <span class="o">*</span> <span class="n">Y</span><span class="p">,</span>  <span class="c1"># X-&gt;</span>
         <span class="n">delta</span> <span class="o">*</span> <span class="n">X</span> <span class="o">*</span> <span class="n">Y</span><span class="p">,</span> <span class="c1"># -&gt;Y</span>
         <span class="n">gamma</span> <span class="o">*</span> <span class="n">Y</span><span class="p">])</span>    <span class="c1"># Y-&gt;</span>
</pre></div>
</div>
<p>Integration using the Gillespie algorithm is undertaken as follows:</p>
<div class="highlight-default notranslate"><div class="highlight"><pre><span></span><span class="nd">@tf</span><span class="o">.</span><span class="n">function</span><span class="p">(</span><span class="n">autograph</span><span class="o">=</span><span class="kc">True</span><span class="p">,</span> <span class="n">experimental_compile</span><span class="o">=</span><span class="kc">True</span><span class="p">)</span>
<span class="k">def</span> <span class="nf">simulate_one</span><span class="p">(</span><span class="n">elems</span><span class="p">):</span>
    <span class="sd">&quot;&quot;&quot; One realisation of epidemic process &quot;&quot;&quot;</span>
    <span class="p">(</span><span class="n">imax</span><span class="p">,</span> <span class="n">times</span><span class="p">,</span> <span class="n">states</span><span class="p">)</span> <span class="o">=</span> <span class="n">integrate_gil</span><span class="p">(</span><span class="n">initial_state</span><span class="p">,</span> <span class="n">rates</span><span class="p">,</span> <span class="n">stoichiometry</span><span class="p">,</span> <span class="n">nsteps</span><span class="p">,</span> <span class="n">propensity</span><span class="p">)</span>
    <span class="k">return</span> <span class="n">tf</span><span class="o">.</span><span class="n">concat</span><span class="p">([</span><span class="n">times</span><span class="p">,</span> <span class="n">states</span><span class="p">],</span> <span class="n">axis</span><span class="o">=</span><span class="mi">1</span><span class="p">)</span>

<span class="n">timeseries</span> <span class="o">=</span> <span class="n">tf</span><span class="o">.</span><span class="n">map_fn</span><span class="p">(</span>
    <span class="n">simulate_one</span><span class="p">,</span>
    <span class="n">tf</span><span class="o">.</span><span class="n">ones</span><span class="p">([</span><span class="n">nsim</span><span class="p">,</span> <span class="mi">1</span><span class="o">+</span><span class="n">stoichiometry</span><span class="o">.</span><span class="n">shape</span><span class="p">[</span><span class="mi">1</span><span class="p">]]),</span>
    <span class="n">fn_output_signature</span><span class="o">=</span><span class="n">dtype</span>
<span class="p">)</span>
</pre></div>
</div>
<p>The resulting simulations may then be plotted:</p>
<div class="highlight-default notranslate"><div class="highlight"><pre><span></span><span class="n">plot_lotka_volterra_gillespie</span><span class="p">(</span><span class="n">timeseries</span><span class="p">,</span> <span class="n">nsim</span><span class="p">)</span>
</pre></div>
</div>
<p>(<a class="reference external" href=".//index-2.py">Source code</a>, <a class="reference external" href=".//index-2.png">png</a>, <a class="reference external" href=".//index-2.hires.png">hires.png</a>, <a class="reference external" href=".//index-2.pdf">pdf</a>)</p>
<figure class="align-default">
<img alt="_images/index-2.png" class="plot-directive" src="_images/index-2.png" />
</figure>
<p>A python script that includes this example is available in the <a class="reference external" href="https://gitlab.com/achale/scm">scm</a> repo, see <em>examples/lotka_volterra_gillespie.py</em> .</p>
</section>
<section id="acknowledgements">
<h1>Acknowledgements<a class="headerlink" href="#acknowledgements" title="Permalink to this headline">¶</a></h1>
<p>Thanks should go to both the Wellcome Trust <em>GEM: translational software for outbreak analysis</em> and also UKRI <em>JUNIPER (Joint UNIversities Pandemic and Epidemiological Research) Consortium</em> for funding this research.</p>
</section>


      </div>
      <div class="bottomnav" role="navigation" aria-label="bottom navigation">
      
        <p>
        <a class="uplink" href="#">Contents</a>
        </p>

      </div>

    <div class="footer" role="contentinfo">
        &#169; Copyright 2021, Alison Hale, Lancaster University.
      Created using <a href="https://www.sphinx-doc.org/">Sphinx</a> 4.1.0.
    </div>
  </body>
</html>