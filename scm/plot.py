
import matplotlib.pyplot as plt
import matplotlib.lines as lines
import tensorflow as tf


def plot_sir_binomial(t, Y, nsteps, nsim):
    """ Plot function for SIR model solved using chain Binomial algorithm
    :param t: (tensor) time increments
    :param Y: (tensor) counts in each state (S, I, R)
    :param nsteps: (integer) number of time steps
    :parma nsim: (integer) number of realisations of the epidemic process
    """
    legend_lines = [lines.Line2D([0], [0], color="#4daf4a", lw=2),
                    lines.Line2D([0], [0], color="#e41a1c", lw=2),
                    lines.Line2D([0], [0], color="#377eb8", lw=2)]
    plt.figure(figsize=(6, 4))
    plt.legend(legend_lines, ["S", "I", "R"])
    for i in range(nsim):
        plt.step(t, Y[:, 0, i], "#4daf4a", lw=0.5, alpha=0.1)
        plt.step(t, Y[:, 1, i], "#e41a1c", lw=0.5, alpha=0.1)
        plt.step(t, Y[:, 2, i], "#377eb8", lw=0.5, alpha=0.1)
    plt.xlabel('time')
    plt.ylabel('state')
    plt.show()


def plot_sir_gillespie(data, nsim):
    """ Plot multiple timeseries for epidemic process
    :param data: (tensor) timeseries of counts for each state (S, I, R)
    :param nsim: (integer) number of realisations of the epidemic process
    """
    # pad tensor with nan where no S,I,R values were computed
    # hence change zero to nan
    dtype = data.dtype
    tY = tf.where(
        tf.expand_dims(
            tf.math.less_equal(
                tf.math.reduce_sum(data, axis=2),
                tf.constant(1E-5,dtype=dtype)),
            axis=2
        ),
        tf.cast(tf.ones_like(data), dtype=dtype) * tf.constant(float('nan'), dtype=dtype),
        data
    ) # catch rows where S(t)=I(t)=R(t)=0
    legend_lines = [lines.Line2D([0], [0], color="#4daf4a", lw=2),
                    lines.Line2D([0], [0], color="#e41a1c", lw=2),
                    lines.Line2D([0], [0], color="#377eb8", lw=2)]
    plt.figure(figsize=(6, 4))
    plt.legend(legend_lines, ["S", "I", "R"])
    for i in range(nsim):
        plt.step(tY[i, :, 0], tY[i, :, 1], "#4daf4a", lw=0.5, alpha=0.1)
        plt.step(tY[i, :, 0], tY[i, :, 2], "#e41a1c", lw=0.5, alpha=0.1)
        plt.step(tY[i, :, 0], tY[i, :, 3], "#377eb8", lw=0.5, alpha=0.1)
    plt.xlabel('time')
    plt.ylabel('state')
    xmax = tf.math.reduce_max(tf.boolean_mask(tY[:, :, 0], tf.math.is_finite(tY[:, :, 0])))
    plt.xlim(-2.0, 2.0 + xmax)
    plt.show()


def plot_lotka_volterra_gillespie(data, nsim):
    """ Plot multiple timeseries for Lotka-Volterra process
    :param data: (tensor) timeseries for X and Y
    :param nsim: (integer) number of realisations of the epidemic process
    """
    # pad tensor with nan where no values were computed hence change zero to nan
    dtype = data.dtype
    tY = tf.where(
        tf.expand_dims(
            tf.math.less_equal(
                tf.math.reduce_sum(data, axis=2),
                tf.constant(1E-5,dtype=dtype)),
            axis=2
        ),
        tf.cast(tf.ones_like(data), dtype=dtype) * tf.constant(float('nan'), dtype=dtype),
        data
    ) # catch rows where X(T)=Y(T)=0
    legend_lines = [lines.Line2D([0], [0], color="#377eb8", lw=2),
                    lines.Line2D([0], [0], color="#e41a1c", lw=2)]
    plt.figure(figsize=(6, 4))
    plt.legend(legend_lines, ["x", "y"])
    for i in range(nsim):
        plt.step(tY[i, :, 0], tY[i, :, 1], "#377eb8", lw=0.5, alpha=0.1)
        plt.step(tY[i, :, 0], tY[i, :, 2], "#e41a1c", lw=0.5, alpha=0.1)
    plt.xlabel('time')
    plt.ylabel('state')
    xmax = tf.math.reduce_max(tf.boolean_mask(tY[:, :, 0], tf.math.is_finite(tY[:, :, 0])))
    ymax = tf.math.reduce_max(tf.boolean_mask(tY[:, :, 2], tf.math.is_finite(tY[:, :, 0])))
    plt.xlim(-0.02, 0.02 + xmax)
    plt.ylim(0, ymax)
    plt.show()
