"""Initialisation functions"""

import tensorflow as tf


def to_tensor(dtype, states, rates, stoichiometry, external):
    """ Convert arrays to tensors
    :param dtype: (dtype) tensorflow dtype
    :param states: (array) initial number of units in each compartment
    :param rates: (array) rates of leaving compartments
    :param stoichiometry: (array) stoichiometry matrix
    :param external: (array) forcing function (e.g. births, immigration, etc.)
    """ 
    rates = tf.ragged.constant(rates, dtype=dtype)
    states = tf.convert_to_tensor([states], dtype=dtype)
    stoichiometry = tf.convert_to_tensor(stoichiometry, dtype=dtype)
    external = tf.convert_to_tensor(external, dtype=dtype)
    return (states, rates, stoichiometry, external)


def initialize_tensors(dtype, states, rates, stoichiometry, external, nsim):
    """ Initialise model tensors
    :param dtype: (dtype) tensorflow dtype
    :param states: (array) initial number of units in each compartment
    :param rates: (array) rates of leaving compartments
    :param stoichiometry: (array) stoichiometry matrix
    :param external: (array) forcing function (e.g. births, immigration, etc.)
    :param nsim: (integer) number of simulations (samples)
    """ 
    (states, rates, stoichiometry, external) = to_tensor(dtype, states, rates, stoichiometry, external)
    Xnr = states.shape[1]
    condMultinom = tf.math.greater(rates.row_lengths(), 1)
    idxsMultinom = tf.squeeze(tf.dtypes.cast(tf.where( condMultinom ),dtype=tf.dtypes.int32), axis=1)
    ratesSum = tf.expand_dims(tf.reduce_sum(rates, axis=1), axis=1)
    relativeRates = tf.math.divide_no_nan(rates, ratesSum)
    relativeRatesFull = tf.where(tf.expand_dims(condMultinom, axis=1),
                                 relativeRates.to_tensor(),
                                 tf.zeros([Xnr, Xnr], dtype=dtype))
    oneMask = tf.where(condMultinom,
                       tf.transpose((tf.math.abs(tf.math.subtract(tf.math.ceil(rates), 1.0))).to_tensor()), 
                       tf.ones([Xnr, Xnr], dtype=dtype))
    stoichiometryBinomial = tf.transpose(tf.broadcast_to(tf.math.multiply(oneMask, stoichiometry), shape=[nsim, Xnr, Xnr]), perm=(2, 0, 1))
    external = tf.broadcast_to(external, shape=[Xnr, nsim])

    return (states, condMultinom, idxsMultinom, ratesSum, relativeRatesFull, stoichiometryBinomial, external)


