from scm import initialize_tensors
from scm import Pr
from scm import integrate_bin
from scm import plot_sir_binomial
import tensorflow as tf
dtype = tf.float64
nsim = 500                  # number of epidemics
scale_dt = 4                # time scale factor
dt = 1.0 / scale_dt         # time step size
nsteps = 100 * scale_dt     # number of time steps
states = [990, 10, 0]       # [S0, I0, R0]
N = sum(states)             # population size
rates = [[0.28],[0.14],[0.0]]
stoichiometry = [[-1.0,  0.0,  0.0], [ 1.0, -1.0,  0.0], [ 0.0,  1.0,  0.0]]
external = [ [0.0],[0.0],[0.0]]
def probs(S, I, R, beta, gamma, zeta, N, dt, nsim):
    return tf.stack([Pr(dt, I * beta / N),tf.broadcast_to(Pr(dt, gamma), [nsim]), tf.broadcast_to(Pr(dt, zeta), [nsim])])
(states, condMultinom, idxsMultinom, ratesSum, relativeRatesFull, stoichiometryBinomial, external) = initialize_tensors(dtype, states, rates, stoichiometry, external, nsim)
(Time, Counts, EventsB, EventsM, EventsP) = integrate_bin(dtype, states, condMultinom, idxsMultinom, ratesSum, relativeRatesFull, stoichiometryBinomial, external, probs, nsteps, N, dt, nsim)
plot_sir_binomial(Time, Counts, nsteps, nsim)
