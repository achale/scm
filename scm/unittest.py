
import matplotlib.pyplot as plt
import tensorflow as tf
import tensorflow_probability as tfp

tfd = tfp.distributions


def r0_distribution_sir(S0, R0, St, It, Rt, N):
    """ Distribution of estimated r0 for SIR model
    :param S0: (tensor) initial number of susceptible individuals
    :param R0: (tensor) initial number of recovered individuals    
    :param St: (tensor) number of susceptible individuals
    :param It: (tensor) number of infected individuals
    :param Rt: (tensor) number of recovered individuals    
    :param N: (integer) total number of individuals (i.e. S+I+R)
    """
    dtype = St.dtype
    r0 = -N * tf.math.log(St / S0) / (Rt - R0)  # r0=beta/gamma
    r0_sim = tf.where(tf.less(r0, 1E-5), tf.constant(float('nan'), dtype=dtype),
                      r0)  # remove zeros since r0 isn't defined when St=S0 n.b. log(st/s0)=0

    fn = lambda i: tf.math.reduce_mean(
        tf.boolean_mask(
            r0_sim[i, ...],
            tf.math.is_finite(r0_sim[i, ...])  # remove non-floats (e.g. inf, nan, ...)
        ))  # expected value per simulation
    r0_mean_per_sim = tf.map_fn(fn=fn, elems=tf.range(r0_sim.shape[0]), fn_output_signature=dtype)
    r0_mean_per_sim = tf.boolean_mask(r0_mean_per_sim, tf.math.is_finite(r0_mean_per_sim))
    qs = tfp.stats.quantiles(r0_mean_per_sim, num_quantiles=4, interpolation='linear')
    plt.hist(r0_mean_per_sim.numpy(), bins=20, color='#CFE2F1', edgecolor='#377eb8', lw=1.0)
    plt.axvline(qs[1].numpy(), color='#000000', linestyle='dashed', lw=1)
    plt.axvline(qs[int((qs.shape[0] - 1) / 2)].numpy(), color='#333333', linestyle='dashed', lw=1)
    plt.axvline(qs[int(qs.shape[0] - 2)].numpy(), color='#000000', linestyle='dashed', lw=1)
    plt.xlim(left=1, right=3)
    plt.xlabel(r'mean $\tilde{\beta}$ / $\tilde{\gamma}$ per simulation')
    plt.ylabel('count')
    plt.show()

    r0_sim_all = tf.boolean_mask(r0_sim, tf.math.is_finite(r0_sim))
    qs = tfp.stats.quantiles(r0_sim_all, num_quantiles=4, interpolation='linear')
    plt.hist(r0_sim_all.numpy(), bins=400, color='#CFE2F1', edgecolor='#377eb8', lw=1.0)
    plt.axvline(qs[1].numpy(), color='#000000', linestyle='dashed', lw=1)
    plt.axvline(qs[int((qs.shape[0] - 1) / 2)].numpy(), color='#333333', linestyle='dashed', lw=1)
    plt.axvline(qs[qs.shape[0] - 2].numpy(), color='#000000', linestyle='dashed', lw=1)
    plt.xlim(left=1, right=3)
    plt.xlabel(r'$\tilde{\beta}$ / $\tilde{\gamma}$ for all simulations and times')
    plt.ylabel('count')
    plt.show()

    return r0_sim, r0_mean_per_sim, r0_sim_all


def pp_distriution(X0, Y0, Xt, Yt, alpha, beta, gamma, delta, param):
    """ Distribution of estimated model parameters (alpha or gamma) for Lokta-Volterra model
    :param X0: (tensor) initial number of prey
    :param Y0: (tensor) initial number of predators    
    :param Xt: (tensor) number of prey
    :param Yt: (tensor) number of predators
    :param alpha: (float) model parameter
    :param beta: (float) model parameter
    :param delta: (float) model parameter
    :param gamma: (float) model parameter
    :param param: (text) name of parameter of interest (n.b either "alpha" or "gamma")
    """
    if param == "alpha":    
      print("actual alpha =", alpha.numpy())
      r0 = ( delta*(Xt-X0) - gamma*tf.math.log(Xt/X0) + beta*(Yt-Y0) ) / tf.math.log(Yt/Y0)
      bins = 200
      plt.xlabel(r'$\alpha$ for all simulations and times')
      vertical_line = alpha
    else:
      print("actual gamma =", gamma.numpy())
      r0 = ( delta*(Xt-X0) - alpha*tf.math.log(Yt/Y0) + beta*(Yt-Y0) ) / tf.math.log(Xt/X0)
      bins = 200
      plt.xlabel(r'$\gamma$ for all simulations and times')
      vertical_line = gamma

    r0_sim = tf.where(tf.less(r0, 1E-5), tf.constant(float('nan'), dtype=Xt.dtype), r0)  # remove zeros
    r0_sim = tf.where(tf.greater(r0_sim, 1.0), tf.constant(float('nan'), dtype=Xt.dtype), r0_sim)

    r0_sim_all = tf.boolean_mask(r0_sim, tf.math.is_finite(r0_sim))

    plt.hist(r0_sim_all.numpy(), bins=bins, color='#CFE2F1', edgecolor='#377eb8', lw=1.0)
    plt.axvline(vertical_line, color='#800000', linestyle='dashed', lw=1)
    plt.xlim(left=0, right=0.4)
    plt.ylabel('count')
    plt.show()

    return r0_sim, r0_sim_all


def r0_report(r0_sim, r0_mean_per_sim, rates):
    dtype = r0_sim.dtype
    standard_error = tf.math.reduce_std(r0_mean_per_sim)/tf.math.sqrt(tf.cast(r0_mean_per_sim.shape[0], dtype=dtype))
    #se_upp = tf.math.reduce_mean(r0_mean_per_sim)+standard_error*se_factor
    #se_low = tf.math.reduce_mean(r0_mean_per_sim)-standard_error*se_factor

    print('\nactual r0 = ', rates[0][0]/rates[1][0])
    tf.print('estimated r0 from overall mean of simulations = ', tf.math.reduce_mean(r0_mean_per_sim))
    tf.print('estimated standard error (se) = ', standard_error)

    tf.print('estimated mean of all r0s = ',
      tf.math.reduce_mean(
          tf.boolean_mask(
              r0_sim, tf.math.is_finite(r0_sim)
          )),'\n') # remove non-floats before computing mean


