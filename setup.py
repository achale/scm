import setuptools

setuptools.setup(
    name='scm',
    version='0.0.2',
    author='Alison Hale',
    author_email='a.c.hale@lancaster.ac.uk',
    description='Stochastic Compartmental Modelling',
    license='MIT',
    url='https://gitlab.com/achale/scm',
    packages=setuptools.find_packages(),
    install_requires=['tensorflow','tensorflow-probability','matplotlib'],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.7',
)
