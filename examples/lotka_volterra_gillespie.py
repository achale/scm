""" Lotka-Volterra (predator–prey) model with Gillespie algorithm

    Model with parameters alpha, beta, delta and gamma:

        dX/dt = alpha X(t) - beta X(t) Y(t)
        dY/dt = delta X(t) Y(t) - gamma Y(t)
        
        X(t) - number of a given prey (e.g. rabbits)
        Y(t) - number of a given predator (e.g. foxes) 

"""

from scm import integrate_gil
from scm import plot_lotka_volterra_gillespie
from scm import pp_distriution

import tensorflow as tf

dtype = tf.float64
nsteps = 30000  # maximum number of time steps
nsim = 200      # number of realisations of Lotka-Volterra process
initial_state = tf.constant([500, 500], dtype=dtype) # initial count for (X,Y)
stoichiometry = tf.constant(
    #  X   Y
    [[ 1,  0],  # ->X
     [-1,  0],  # X->
     [ 0,  1],  # ->Y
     [ 0, -1]], # Y->
     dtype=dtype
)

rates = tf.constant(
    [[0.2],    # ->X
     [0.0005], # X->
     [0.0005], # ->Y
     [0.1]],   # Y->
    dtype=dtype)

def propensity(X, Y, alpha, beta, delta, gamma, N):
    """ Propensity for Lotka-Volterra model
    :param X: (tensor) number of a given prey
    :param Y: (tensor) number of a given predator
    :param alpha: (float) model parameter
    :param beta: (float) model parameter
    :param delta: (float) model parameter
    :param gamma: (float) model parameter
    :param N: (integer): total population size
    """
    return tf.stack(
        [alpha * X,     # ->X
         beta * X * Y,  # X->
         delta * X * Y, # ->Y
         gamma * Y])    # Y->


# integrate using Gillespie algorithm
@tf.function(autograph=True, experimental_compile=True)
def simulate_one(elems):
    """ One realisation of epidemic process """
    (imax, times, states) = integrate_gil(initial_state, rates, stoichiometry, nsteps, propensity)
    return tf.concat([times, states], axis=1)

timeseries = tf.map_fn(
    simulate_one,
    tf.ones([nsim, 1+stoichiometry.shape[1]]),
    fn_output_signature=dtype
)


# plot function for spcifically for Lotka-Volterra model
plot_lotka_volterra_gillespie(timeseries, nsim)


# unit tests based on the distribution of each estimated model parameter
X0 = initial_state[-2]
Y0 = initial_state[-1]
Xt = timeseries[:, :, -2]
Yt = timeseries[:, :, -1]
alpha = rates[0,0]
beta  = rates[1,0]
delta = rates[2,0]
gamma = rates[3,0]

# when the Gillespie algorithm is working correctly the actual value of each
# model parameter will be very close to the mode of the 
# distribution of the estimated distribution for the respective paramater
_ = pp_distriution(X0, Y0, Xt, Yt, alpha, beta, gamma, delta, "alpha") # alpha distr.
_ = pp_distriution(X0, Y0, Xt, Yt, alpha, beta, gamma, delta, "gamma") # gamma distr.

