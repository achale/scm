""" Gillespie algorithm """

import tensorflow as tf
import tensorflow_probability as tfp

tfd = tfp.distributions


def discrete_sample(dtype, probs):
    """ Discrete sample - randomly sample an index given a probability tensor (probs).
    :param dtype: (dtype) tensorflow dtype
    :param probs: (tensor) transition probabilities

    Algorithm using numpy for readability:
            import numpy as np
            pr=np.array([0.1, 0.2 , 0.5, 0.2]); rn=0.15; i=0; cum_sum=0.0
            while cum_sum < rn:
                cum_sum += pr[i]
                i += 1
            print('index = ', i - 1)
    """
    rn = tfd.Uniform().sample()
    cum_sum = tf.cumsum(probs)
    truth = tf.where(
        tf.math.less(cum_sum, tf.cast(rn, dtype=dtype)),
        tf.constant(1),
        tf.constant(0)
    )
    return tf.math.reduce_sum(truth)    # return an index as integer


@tf.function(autograph=True, experimental_compile=True)
def integrate_gil(initial_state, rates, stoichiometry, nsteps, propensity):
    """ General Gillespie algorithm for N compartments
    :param initial_state: (tensor) initial number of units in each compartment
    :param rates: (tensor) transition rates
    :param stoichiometry: (tensor) stoichiometry matrix
    :param nsteps: (integer) maximum number of time steps
    """
    def body(i, T, tn, X, Xn):
        Sn = tf.unstack(Xn) + rates   # S, I, R, beta, gamma
        props = propensity(*Sn, N)    # propensity
        props_sum = tf.math.reduce_sum(props, axis=0)
        pr = props / tf.expand_dims(props_sum, axis=1) # transition probabilities
        index = discrete_sample(dtype, pr)
        Xm = tf.math.add(
            Xn, 
            tf.gather(stoichiometry, index)
        ) # next state
        tm = tf.math.add(
            tn,
            tfd.Exponential(props_sum).sample()
        ) # next time
        return (i+1, T.write(i, tm), tm, X.write(i, Xm), Xm)

    #def cond(i, T, tn, X, Xn): # Only applicable to SIR
    #    return tf.math.greater(
    #        tf.math.floor(Xn[1]),
    #        tf.constant(1.0E-5, dtype=dtype)
    #    ) # if I > 0 then True otherwise False
    def cond(i, T, tn, X, Xn): # applicable to all models
        return True 

    dtype = initial_state.dtype
    N = tf.math.reduce_sum(initial_state)
    rates = tf.unstack(rates)
    T0 = tf.zeros(1, dtype=dtype)
    X0  = initial_state
    T = tf.TensorArray(dtype=dtype, size=nsteps, element_shape=T0.shape) # times
    X = tf.TensorArray(dtype=dtype, size=nsteps, element_shape=X0.shape) # states

    (imax, times, tmax, states, xmax) = tf.while_loop(
        cond=cond,
        body=body, 
        loop_vars=(tf.constant(1), T.write(0, T0), T0, X.write(0, X0), X0),
        maximum_iterations = nsteps
    )
    return (imax, times.stack(), states.stack())

