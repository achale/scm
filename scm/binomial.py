"""Chain Binomial Algorithm"""

import tensorflow as tf
import tensorflow_probability as tfp

tfd = tfp.distributions


def Pr(dt, rate):
    """ Probability of leaving a compartment
    :param dt: (float) size of time step
    :param rate: (tensor) rate of leaving a compartment
    """  
    return 1 - tf.math.exp(-rate * dt)


@tf.function(autograph=True, experimental_compile=False)
def integrate_bin(dtype, states, condMultinom, idxsMultinom, ratesSum, relativeRatesFull, stoichiometryBinomial, external, probs, nsteps, N, dt, nsim):
    """ General chain binomial algorithm for N compartments 
    :param dtype: tensorflow dtype    
    :param states: (tensor) initial number of units in each compartment
    :param condMultinom: (boolean) which compartments have multiple branches
    :param idxsMultinom: (tensor) indexes of compartments with multiple branches
    :param ratesSum: (tensor) sum of rates leaving each compartment
    :param relativeRatesFull: (tensor) relative rate of leaving a given compartment with multiple branches
    :param stoichiometryBinomial: (tensor) stoichiometry matrix with multinomial parts taken out
    :param external: (tensor) forcing term (e.g. births, immigration,...)
    :param probs: function returning transition probabilities for each compartment
    :param nsteps: (integer) number of time steps
    :param N: (integer) total number of people in population
    :param dt: (float) size of time step
    :param nsim: (integer) number of simulations (samples)
    """  
    def body(t, X, Xn, Eb, Em, Ep):
        Sn = tf.unstack(Xn) + unstackedratesSum # X_1 ... X_n, p_1 ... p_k = S, I, R, beta, gamma, zeta
        Pn = probs(*Sn, N, dt, nsim) # transition probabilities
        Xbin = tfd.Binomial(total_count=Xn, probs=Pn).sample()
        updateXbinomial = tf.math.multiply(stoichiometryBinomial, tf.expand_dims(Xbin, axis=2))

        updateXmultinomial = tf.TensorArray(dtype=dtype, size=Xnr, element_shape=[nsim,Xnr])
        for j in range(Xnr):  # with range() autograph rolls out the for loop - with tf.range autograph creates a while loop
            Xmuls = tf.cond(condMultinom[j],
                          lambda: tfd.Multinomial(total_count=Xbin[j,], probs=relativeRatesFull[j,]).sample(),
                          lambda: tf.zeros([nsim,Xnr], dtype=dtype))
            updateXmultinomial = updateXmultinomial.write(j, Xmuls)       
        Xmul = updateXmultinomial.stack()
        updateX = tf.math.add(Xmul, updateXbinomial)
   
        Xpoi = tfd.Poisson(rate=external).sample()
        Xm = tf.math.add_n([Xn, tf.transpose(tf.math.reduce_sum(updateX, axis=0)), Xpoi])
        return (tf.math.add(t, 1), X.write(t, Xm), Xm, Eb.write(t-1, Xbin), Em.write(t-1, Xmul), Ep.write(t-1, Xpoi))

    def cond(t, X, Xn, Eb, Em, Ep):
        return tf.math.less(t, nsteps)

    unstackedratesSum = tf.unstack(ratesSum)
    Xnr = states.shape[1]
    zs =  tf.zeros([Xnr,nsim], dtype=dtype)
    X0  = tf.transpose(tf.broadcast_to(states, shape=[nsim, Xnr]))
    X = tf.TensorArray(dtype=dtype, size=nsteps, element_shape=X0.shape)         # total count in every state at each time slice
    Eb = tf.TensorArray(dtype=dtype, size=nsteps-1, element_shape=zs.shape)      # number of binomial events per dt at each time slice
    Em0 = tf.zeros([Xnr,nsim,Xnr], dtype=dtype)                                  
    Em = tf.TensorArray(dtype=dtype, size=nsteps-1, element_shape=Em0.shape)     # number multinomial events per dt at each time slice
    Ep = tf.TensorArray(dtype=dtype, size=nsteps-1, element_shape=zs.shape)      # number poisson events per dt at each time slice
    (Tend, Xs, Xn, Ebs, Ems, Eps) = tf.while_loop(cond=cond,
                                                 body=body, 
                                                 loop_vars=(tf.constant(1), X.write(0, X0), X0, Eb.write(0, zs), Em.write(0, Em0), Ep.write(0, zs) ) )

    return ( tf.expand_dims(tf.range(0.0, nsteps, 1.0)*dt, axis=1), Xs.stack(), Ebs.stack(), Ems.stack(), Eps.stack() )

